using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float velocidade;
    public float velPulo;
    private Rigidbody2D rb;
    public bool podePular;
    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        if(x != 0)
        {
            rb.velocity = new Vector2(velocidade * x, rb.velocity.y);
        }
        if(Input.GetKeyDown(KeyCode.Space) && podePular == true)
        {
            podePular = false;
            rb.velocity = new Vector2(rb.velocity.x, velPulo);
        }
        /*RaycastHit2D hit;
        hit = Physics2D.Raycast(transform.position, new Vector2(0, 0.1f));
        while(hit == true)
        {
            podePular = true;
        }*/
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "chao")
        {
            podePular = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
    }

}
